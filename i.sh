#!/bin/bash
#
# Auto Installer for Ubuntu Server
#
# Copyright (C) 2017-2018 MaXi
#
# This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0
# Unported License: http://creativecommons.org/licenses/by-nc-nd/4.0/
#
# Testing was done on Ubuntu 16.04.4 LTS
# cd && wget http://bitbucket.org/raletag/ubuntu-16.04-install/raw/master/i.sh -O i.sh && sudo bash i.sh && sudo rm i.sh
#


##################################################
clear
echo "###START##########################################"
RAM_SIZE=$(awk '/^MemTotal:/ {printf("%d", $2)}' /proc/meminfo)
PHP_VER='7.1'
MARIADB_VER='10.3'

sudo timedatectl set-timezone Europe/Moscow
sudo dpkg-reconfigure --frontend noninteractive tzdata

sudo apt update
sudo apt -y autoremove
sudo apt install -y language-pack-en-base
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
sudo apt install -y software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository "deb [arch=amd64] http://mirror.timeweb.ru/mariadb/repo/$MARIADB_VER/ubuntu xenial main"
sudo add-apt-repository -y ppa:ondrej/php
sudo add-apt-repository -y ppa:ondrej/apache2
sudo add-apt-repository -y ppa:ondrej/nginx
sudo add-apt-repository -y ppa:nijel/phpmyadmin
sudo apt update
sudo apt -y upgrade
sudo apt install -y curl zip fail2ban htop byobu telnet
sudo date --set=@$(curl -s https://time.akamai.com/)

if [ ! -f /var/log/mysql/password ]; then
    DB_PASSWORD=$(head -c 255 /dev/urandom | tr -dc A-HJ-NPR-Za-km-z2-9 | head -c 14)
    sudo mkdir -p /var/log/mysql
    sudo touch /var/log/mysql/password
    sudo chown root /var/log/mysql/password
    sudo chmod 600 /var/log/mysql/password
    sudo echo "$DB_PASSWORD" > /var/log/mysql/password
else
    DB_PASSWORD=$(</var/log/mysql/password)
fi
sudo ln -sf /etc/crontab /var/crontab
SERVER_IP=$(curl -s http://whatismyip.akamai.com/)
echo "IP: $SERVER_IP"
echo "##################################################"
##################################################

##################################################
sudo apt install -y ufw
#sudo ufw --force reset
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw allow 33060/tcp
sudo ufw --force enable
echo "##################################################"
##################################################

##################################################
PHP_INI="/etc/php/$PHP_VER/fpm/php.ini"
sudo apt install -y php$PHP_VER php$PHP_VER-fpm php$PHP_VER-cli php$PHP_VER-opcache php$PHP_VER-mbstring php$PHP_VER-curl php$PHP_VER-mcrypt php$PHP_VER-json php$PHP_VER-gmp php$PHP_VER-bcmath php$PHP_VER-gd
sudo apt install -y php$PHP_VER-zip php$PHP_VER-mysqli php$PHP_VER-mongodb php$PHP_VER-xml php$PHP_VER-imap php$PHP_VER-bz2 php$PHP_VER-memcached php$PHP_VER-phpdbg

sudo perl -i -pe's/(;|)\s*expose_php\s*=.*/expose_php = Off/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*cgi\.fix_pathinfo\s*=.*/cgi.fix_pathinfo = 0/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*post_max_size\s*=.*/post_max_size = 64M/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*upload_max_filesize\s*=.*/upload_max_filesize = 64M/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*max_execution_time\s*=.*/max_execution_time = 300/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.enable\s*=.*/opcache.enable = 1/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.enable_cli\s*=.*/opcache.enable_cli = 1/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.memory_consumption\s*=.*/opcache.memory_consumption = 128/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.interned_strings_buffer\s*=.*/opcache.interned_strings_buffer = 4/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.max_accelerated_files\s*=.*/opcache.max_accelerated_files = 7963/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.validate_timestamps\s*=.*/opcache.validate_timestamps = 1/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*opcache\.revalidate_freq\s*=.*/opcache.revalidate_freq = 30/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*error_reporting\s*=.*/error_reporting = E_ALL & ~E_NOTICE/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*log_errors\s*=.*/log_errors = On/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*max_input_vars\s*=.*/max_input_vars = 4096/g' $PHP_INI
#sudo perl -i -pe's/(;|)\s*open_basedir\s*=.*/open_basedir = \/var\/www/g' $PHP_INI
sudo perl -i -pe"s/(;|)\s*error_log\s*=.*\.log/error_log = \/var\/log\/php$PHP_VER-fpm.www.log/g" $PHP_INI
sudo perl -i -pe's/(;|)\s*allow_url_fopen\s*=.*/allow_url_fopen = Off/g' $PHP_INI
sudo perl -i -pe's/(;|)\s*request_slowlog_timeout\s*=.*/request_slowlog_timeout = 30s/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe"s/(;|)\s*slowlog\s*=.*/slowlog = \/var\/log\/php$PHP_VER-fpm.www.slow.log/g" /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*pm\.max_requests\s*=.*/pm.max_requests = 100/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*pm\.max_children\s*=.*/pm.max_children = 12/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*pm\.start_servers\s*=.*/pm.start_servers = 6/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*pm\.min_spare_servers\s*=.*/pm.min_spare_servers = 3/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*pm\.max_spare_servers\s*=.*/pm.max_spare_servers = 6/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf
sudo perl -i -pe's/(;|)\s*request_terminate_timeout\s*=.*/request_terminate_timeout = 600s/g' /etc/php/$PHP_VER/fpm/pool.d/www.conf

sudo touch /var/log/php$PHP_VER-fpm.www.slow.log
sudo chown -f www-data /var/log/php$PHP_VER-fpm.www.slow.log
sudo touch /var/log/php$PHP_VER-fpm.www.log
sudo chown -f www-data /var/log/php$PHP_VER-fpm.www.log
sudo service php$PHP_VER-fpm restart
echo "##################################################"
##################################################

##################################################
sudo apt install -y apache2 libapache2-mod-fastcgi libapache2-mod-rpaf

sudo touch /etc/apache2/ports.conf
sudo echo '
Listen 8080
' > /etc/apache2/ports.conf

sudo touch /etc/apache2/sites-available/000-default.conf
sudo echo "
<VirtualHost *:8080>
	ServerName $SERVER_IP
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html
	<Directory /var/www/html>
        AllowOverride All
    </Directory>
</VirtualHost>
" > /etc/apache2/sites-available/000-default.conf
sudo a2ensite 000-default.conf

sudo touch /etc/apache2/mods-enabled/fastcgi.conf
sudo echo "
<IfModule mod_fastcgi.c>
	AddType application/x-httpd-fastphp .php
	Action application/x-httpd-fastphp /php-fcgi
	Alias /php-fcgi /usr/lib/cgi-bin/php-fcgi
	FastCgiExternalServer /usr/lib/cgi-bin/php-fcgi -socket /run/php/php$PHP_VER-fpm.sock -pass-header Authorization
	<Directory /usr/lib/cgi-bin>
		Require all granted
	</Directory>
 </IfModule>
" > /etc/apache2/mods-enabled/fastcgi.conf

sudo touch /etc/apache2/conf-available/security.conf
sudo echo '
ServerTokens Prod
ServerSignature Off
TraceEnable Off
' > /etc/apache2/conf-available/security.conf

sudo touch /etc/apache2/conf-available/charset.conf
sudo echo '
AddDefaultCharset utf-8
' > /etc/apache2/conf-available/charset.conf

sudo touch /etc/apache2/apache2.conf
sudo echo '
DefaultRuntimeDir ${APACHE_RUN_DIR}
PidFile ${APACHE_PID_FILE}
User ${APACHE_RUN_USER}
Group ${APACHE_RUN_GROUP}

HostnameLookups Off
AccessFileName .htaccess
Timeout 300

KeepAlive Off
MaxConnectionsPerChild 1000

LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent

ErrorLog ${APACHE_LOG_DIR}/error.log
LogLevel warn

Include ports.conf
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
IncludeOptional conf-enabled/*.conf
IncludeOptional sites-enabled/*.conf
' > /etc/apache2/apache2.conf

sudo touch /etc/apache2/mods-available/rpaf.conf
sudo echo '
<IfModule mod_rpaf.c>
    RPAF_Enable             On
    RPAF_Header             X-Real-Ip
    RPAF_ProxyIPs           127.0.0.1
    RPAF_SetHostName        On
    RPAF_SetHTTPS           On
    RPAF_SetPort            On
</IfModule>
' > /etc/apache2/mods-available/rpaf.conf

sudo touch /etc/apache2/conf-available/other-vhosts-access-log.conf
sudo echo '
#CustomLog ${APACHE_LOG_DIR}/access.log combined
' > /etc/apache2/conf-available/other-vhosts-access-log.conf

sudo touch /etc/apache2/mods-available/mpm_event.conf
sudo echo '
# event MPM

<IfModule mpm_event_module>
    ServerLimit 2
    StartServers 1
    MinSpareThreads 25
    MaxSpareThreads 75
    ThreadsPerChild 25
    MaxRequestWorkers 50
    MaxRequestsPerChild 0
    GracefulShutdownTimeout 5
</IfModule>
' > /etc/apache2/mods-available/mpm_event.conf

sudo ln -sf /etc/apache2 /var/apache2

sudo a2dismod status mpm_prefork mpm_worker
sudo a2enmod actions rpaf rewrite mpm_event

sudo service apache2 restart
##################################################

##################################################
sudo apt install -y nginx
sudo rm -f /var/www/html/index.nginx-debian.html

sudo touch /etc/nginx/nginx.conf
sudo echo '
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
worker_rlimit_nofile 65536;
timer_resolution 100ms;
worker_priority -5;

events {
	worker_connections 2048;
	multi_accept on;
}

http {
	sendfile on;
	sendfile_max_chunk 1m;
	tcp_nopush on;
	tcp_nodelay on;
	directio 10m;
	limit_rate_after 1m;
	limit_rate 512k;
	keepalive_timeout 30s;
	keepalive_requests 100;
	types_hash_max_size 2048;
	server_tokens off;
	open_file_cache max=4096 inactive=30s;
	open_file_cache_valid 30s;
	open_file_cache_min_uses 2;
	open_file_cache_errors on;
	output_buffers 4 32k;

	reset_timedout_connection on;
    client_body_buffer_size 32k;
    client_header_buffer_size 4k;
    large_client_header_buffers 8 16k;
	client_header_timeout 10s;
	client_body_timeout 10s;
	send_timeout 10s;
	client_max_body_size 65k;
    index index.php index.html;

	fastcgi_send_timeout 60;
	fastcgi_read_timeout 60;

	# server_names_hash_bucket_size 64;
	# server_name_in_redirect off;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	charset UTF-8;

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	log_format default $time_local|$remote_addr>$realip_remote_addr>$host|$status|$request_time/$upstream_response_time|$request_length|$request|$http_user_agent|$http_referer;
	error_log /var/log/nginx/error.log crit;
	access_log /var/log/nginx/access.log default buffer=32k flush=60s;

	gzip on;
	gzip_static off;
	gzip_disable "msie6";
	gzip_vary off;
	gzip_proxied any;
	gzip_comp_level 4;
	gzip_buffers 16 8k;
	gzip_min_length 512;
	gzip_http_version 1.0;
	gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
' > /etc/nginx/nginx.conf

sudo touch /etc/nginx/conf.d/00-real-ip-cloudflare.conf
sudo truncate -s 0 /etc/nginx/conf.d/00-real-ip-cloudflare.conf
for ip in `curl -s https://www.cloudflare.com/ips-v4` `curl -s https://www.cloudflare.com/ips-v6`; do
	sudo echo "set_real_ip_from $ip;" >> /etc/nginx/conf.d/00-real-ip-cloudflare.conf
done
sudo echo "real_ip_header CF-Connecting-IP;" >> /etc/nginx/conf.d/00-real-ip-cloudflare.conf

sudo touch /etc/nginx/sites-available/default
sudo echo '
server {
    listen 80 default_server;
	listen [::]:80 default_server; 
	listen 33060 default_server; 
	listen [::]:33060 default_server;  
	server_name _;
	return 444;
}
' > /etc/nginx/sites-available/default
sudo ln -sf /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

sudo touch /etc/nginx/sites-available/ip
sudo echo "
server {
    listen 80;
	listen [::]:80; 
	server_name $SERVER_IP;
	root /var/www/html;
	include snippets/default.conf;
	
	location /server_status {
		stub_status;
	}
}
" > /etc/nginx/sites-available/ip
sudo ln -sf /etc/nginx/sites-available/ip /etc/nginx/sites-enabled/ip

sudo touch /etc/nginx/snippets/firewall.conf
sudo echo '
	if ($request_method !~ ^(GET|POST)$ ) {
		return 444;
	}
	
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
		return 404;
	}
' > /etc/nginx/snippets/firewall.conf

sudo touch /etc/nginx/snippets/php$PHP_VER.conf
sudo echo "
	location ~ \.php\$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)\$;
		try_files \$fastcgi_script_name =404;
		include fastcgi_params;
		fastcgi_index index.php;
		fastcgi_param PATH_INFO \$fastcgi_path_info;
		fastcgi_pass unix:/var/run/php/php$PHP_VER-fpm.sock;
		client_max_body_size 65m;
	}
" > /etc/nginx/snippets/php$PHP_VER.conf

sudo touch /etc/nginx/snippets/log-off.conf
sudo echo '
	location = /robots.txt {
		log_not_found off;
	}

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

	location ~ /apple-touch-icon(|-\d+x\d+)(|-precomposed).png {
		log_not_found off;
		access_log off;
	}
' > /etc/nginx/snippets/log-off.conf

sudo touch /etc/nginx/snippets/default.conf
sudo echo "
	include snippets/firewall.conf;
	include snippets/php$PHP_VER.conf;
	include snippets/log-off.conf;
" > /etc/nginx/snippets/default.conf

sudo touch /etc/nginx/snippets/apache2.conf
sudo echo '
	include snippets/firewall.conf;
	include snippets/log-off.conf;

	location / {
		proxy_pass http://127.0.0.1:8080;
		include proxy_params;
		client_max_body_size 65m;
	}
' > /etc/nginx/snippets/apache2.conf

sudo touch /etc/nginx/snippets/apache2-php.conf
sudo echo '
	include snippets/firewall.conf;
	include snippets/log-off.conf;

	location ~ \.php$ {
		proxy_pass http://127.0.0.1:8080;
		include proxy_params;
		client_max_body_size 65m;
	}
' > /etc/nginx/snippets/apache2-php.conf

sudo touch /etc/nginx/proxy_params
sudo echo '
proxy_set_header Host $host;
proxy_set_header Connection "";
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $scheme;
proxy_pass_header Set-Cookie;
proxy_pass_header Server;
proxy_http_version 1.1;
' > /etc/nginx/proxy_params

sudo touch /etc/nginx/fastcgi_params
sudo echo '
fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;

fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;
fastcgi_param  REQUEST_SCHEME     $scheme;
fastcgi_param  HTTPS              $https if_not_empty;

fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;

# PHP only, required if PHP was built with --enable-force-cgi-redirect
fastcgi_param  REDIRECT_STATUS    200;
' > /etc/nginx/fastcgi_params

sudo touch /var/www/html/index.html
sudo echo '<!DOCTYPE html>
<html>
	<head>
		<title>Hello, world!</title>
	</head>
	<body>
		<h1>Hello, world!</h1>
	</body>
</html>
' > /var/www/html/index.html

sudo touch /var/www/html/info.php
sudo echo "<?php
 // Copyright (C) 2017-2018 MaXi
 // CC BY-NC-ND 4.0 http://creativecommons.org/licenses/by-nc-nd/4.0/
 error_reporting(-1);
 \$i = 0;
 \$t = time();
 while (time() == \$t);
 ++\$t;
 while (time() == \$t) ++\$i;
 echo 'PHP '.PHP_MAJOR_VERSION.'.'.PHP_MINOR_VERSION.'.'.PHP_RELEASE_VERSION.' - '.number_format(\$i, 0, ',', ' ');
" > /var/www/html/info.php

sudo ln -sf /etc/nginx /var/nginx

sudo mkdir -p /var/log/nginx
sudo touch /var/log/nginx/access.log
sudo touch /var/log/nginx/error.log
sudo chown www-data:adm /var/log/nginx
sudo chown www-data:adm /var/log/nginx/access.log
sudo chown www-data:adm /var/log/nginx/error.log 
sudo service nginx restart
echo "##################################################"
##################################################

##################################################
sudo debconf-set-selections <<< "maria-db-$MARIADB_VER mysql-server/root_password password $DB_PASSWORD"
sudo debconf-set-selections <<< "maria-db-$MARIADB_VER mysql-server/root_password_again password $DB_PASSWORD"
sudo apt install -y mariadb-server
sudo mkdir -p /var/log/mysql
sudo touch /var/log/mysql/error.log
sudo chown mysql /var/log/mysql
sudo chown mysql /var/log/mysql/error.log
sudo service mariadb restart
echo "##################################################"
##################################################

##################################################
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/configuring-phpmyadmin boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-user string root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DB_PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DB_PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DB_PASSWORD"
sudo apt install -y phpmyadmin
sudo cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php
sudo perl -i -pe"s/cfg\['blowfish_secret'\].*/cfg['blowfish_secret'] = '"`(head -c 255 /dev/urandom | tr -dc A-Za-z0-9 | head -c 36)`"';/g" /usr/share/phpmyadmin/config.inc.php
sudo touch /etc/nginx/sites-available/phpmyadmin
sudo echo "
server {
    listen 33060;
	listen [::]:33060; 
	server_name $SERVER_IP;
	root /usr/share/phpmyadmin;
	fastcgi_read_timeout 300;
	include snippets/default.conf;
}
" > /etc/nginx/sites-available/phpmyadmin
sudo ln -sf /etc/nginx/sites-available/phpmyadmin /etc/nginx/sites-enabled/phpmyadmin
echo "##################################################"
##################################################

##################################################
let MEMCACHED_SIZE=$((RAM_SIZE))/10240+32
sudo apt install -y memcached
sudo echo "
# Run memcached as a daemon.
-d

# Log memcached's output to /var/log/memcached
logfile /var/log/memcached.log

# Be verbose
# -v

# Be even more verbose (print client commands as well)
# -vv

# Start with a cap of 64 megs of memory. It's reasonable, and the daemon default
# Note that the daemon will grow to this size, but does not start out holding this much
# memory
-m $MEMCACHED_SIZE

# Default connection port is 11211
-p 11211

# Run the daemon as root. The start-memcached will default to running as root if no
# -u command is present in this config file
-u memcache

# Specify which IP address to listen on. The default is to listen on all IP addresses
# This parameter is one of the only security measures that memcached has, so make sure
# it's listening on a firewalled interface.
-l 127.0.0.1

# Limit the number of simultaneous incoming connections. The daemon default is 1024
-c 1024

# Lock down all paged memory. Consult with the README and homepage before you do this
# -k

# Return error when memory is exhausted (rather than removing items)
# -M

# Maximize core file limit
# -r
" > /etc/memcached.conf
sudo touch /var/log/memcached.log
sudo service memcached restart
echo "##################################################"
##################################################

##################################################
sudo swapoff -a
sudo rm -rf /swap
sudo fallocate -l $(($((RAM_SIZE))*512+33554433)) /swap
sudo chmod 600 /swap
sudo mkswap /swap
sudo swapon /swap
sudo grep -q -F '/swap ' /etc/fstab || echo '/swap none swap sw 0 0' >> /etc/fstab
sudo sysctl -w vm.swappiness=20
sudo sysctl -w vm.vfs_cache_pressure=50
sudo free -h
echo "##################################################"
##################################################

##################################################4
sudo service cron restart
sudo service php$PHP_VER-fpm restart
sudo service apache2 restart
sudo service nginx restart
sudo service mariadb restart
sudo service memcached restart
##################################################

##################################################
echo
echo "##################################################"
echo "   phpMyAdmin:   http://$SERVER_IP:33060/"
echo "   USER:         root"
echo "   PASSWORD:     $DB_PASSWORD"
echo "##################################################"
echo
##################################################

##################################################
#sudo apt install -y linux-generic-hwe-16.04-edge
#sudo dpkg --list | grep linux-image | awk '{ print $2 }' | sort -V | sed -n '/'`uname -r`'/q;p' | xargs sudo apt-get -y purge
sudo apt clean
echo "###FINISH#########################################"
#sudo reboot
##################################################